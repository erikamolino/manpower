import React from 'react';
import './AddProfileForm.css';

class AddProfileForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            jobTitle: '',
            type: '',
            positionVacant: '',
            SLO: ''
        }
    }

 
    handleChange = event => {
        const name = event.target.name
        const value = event.target.type === 'checkbox' ? event.target.checked : event.target.value;

        this.setState({
            [name]: value
        })
    }

    handleSubmit = event => {
        event.preventDefault();

        if (this.state.jobTitle && this.state.type && this.state.positionVacant && this.state.SLO) {
            this.props.addNewProfile({
                ...this.state,
                id: this.props.arrayLength
            });
            this.props.toggleShowForm();
        } else {
            alert('Devi compilare tutti i campi')
        }

    }



    render() {
        console.log(this.state)
        return (
            <form className='AddProfileForm' onSubmit={this.handleSubmit}>
                <div>
                    <input
                        type="text"
                        name='jobTitle'
                        placeholder='Insert job title'
                        onChange={this.handleChange}
                        value={this.state.jobTitle}
                    />
                </div>
    
                <div>
                    <select
                        name='type'
                        onChange={this.handleChange}
                        value={this.state.type}
                    >
                        <option value="">Select a type</option>
                        <option value="core">Core</option>
                        <option value="rare">Rare</option>
                    </select>
                </div>
    
                <div>
                    <input
                        type="number"
                        name='positionVacant'
                        placeholder='Insert position vacant'
                        min={0}
                        onChange={this.handleChange}
                        value={this.state.positionVacant}
                    />
                </div>
    
                <div>
                    <input
                        type="checkbox"
                        name='SLO'
                        onChange={this.handleChange}
                        value={this.state.SLO}
                    />
                    <span>
                        Staff leasing opportunity
                    </span>
                </div>
    
                <button type='submit'>
                    ADD
                </button>
            </form>
        )
    }
}


export default AddProfileForm;