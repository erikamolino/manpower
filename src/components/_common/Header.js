import React from 'react';
import './Header.css';

class Header extends React.Component {
    getLi = () => (
        this.props.navConfig.map((item, index) => (
            <li key={index}>{item.title}</li>
        ))
    )
    
    render() {
        return (
            <header>
                <div className='imgContainer'>
                    <img src={this.props.logo} alt=""/>
                </div>
    
                <nav>
                    <ul>
                        {this.getLi()}
                    </ul>
                </nav>
            </header>
        )
    }
}

export default Header;