import React from 'react';
import Lottie from 'react-lottie';
import animationData from './delete.json'

const defaultOptions = {
    loop: false,
    autoplay: true, 
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice'
    }
};

const CheckAnimated = props => {
    return (
        <Lottie
            options={defaultOptions}
            height={64}
            width={64}
        />
    )
}

export default CheckAnimated;
