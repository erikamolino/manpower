import React from 'react'

const Button = props => {
    return (
        <button onClick={props.handleClick}>
            {props.text.toUpperCase()}
        </button>
    )
}

export default Button;