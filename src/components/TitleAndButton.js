import React from 'react';
import './TitleAndButton.css';
import Button from './_common/Button'

const TitleAndButton = props => {
    return (
        <div className='TitleAndButton'>
            <h3>{props.title}</h3>

            <Button
                handleClick={props.toggleShowForm}
                text={props.buttonText}
            />
        </div>
    )
}

export default TitleAndButton;