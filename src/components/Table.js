import React from 'react';
import './Table.css';
import CheckAnimated from './_common/CheckAnimated';
import XAnimated from './_common/XAnimated';
import { thisTypeAnnotation } from '@babel/types';
import Button from './_common/Button';

class Table extends React.Component {
    deleteProfile = (id) => () => {
        this.props.deleteProfile(id);
    }

    getRow = () => {
        const arrayOfRow = this.props.profiles.map((item, index) => {
            return (
                <tr key={index}>
                    <td>{item.id}</td>
                    <td>{item.jobTitle}</td>
                    <td>{item.type}</td>
                    <td>{item.positionVacant}</td>
                    <td>{item.SLO ? <CheckAnimated /> : <XAnimated />}</td>
                    <td>
                        <Button
                            text='caNceLla'
                            handleClick={this.deleteProfile(item.id)}
                        />
                    </td>
                </tr>
            )
        });
        return arrayOfRow;
    }
 
    render() {
        return (
            <div className='Table'>
                <table>
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>jobTitle</th>
                            <th>type</th>
                            <th>positionVacant</th>
                            <th>SLO</th>
                            <th></th>
                        </tr>
                    </thead>
    
                    <tbody>
                        {this.getRow()}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default Table;