import React, { Component } from 'react';
import TitleAndButton from './TitleAndButton';
import Table from './Table';
import AddProfileForm from './AddProfileForm';

const defaultProfiles = [{
    id: 0,
    jobTitle: 'React developer',
    type: 'core',
    positionVacant: 10,
    SLO: false
}, {
    id: 1,
    jobTitle: 'Java developer',
    type: 'rare',
    positionVacant: 200,
    SLO: true
}];

const exampleProfile = {
    id: 1,
    jobTitle: 'Panettiere',
    type: 'rare',
    positionVacant: 322,
    SLO: false
}

export default class Profiles extends Component {
    constructor(props) {
        super(props);
        this.state = {
            profiles: [],
            showForm: false
        }
    }

    componentDidMount() {
        this.setState({
            profiles: defaultProfiles
        })
    }

    deleteProfile = id => {
        this.setState({
            profiles: this.state.profiles.filter(item => !(id === item.id))
        });
    }
    
    addNewProfile = newProfile => {
        this.setState(prevState => {
            const newProfiles = [
                ...prevState.profiles,
                newProfile
            ]
            return {
                profiles: newProfiles
            }
        });
    }

    toggleShowForm = () => {
        this.setState(prevState => {
            return {
                showForm: !prevState.showForm   
            }
        })
    }
    
    render() {
        return (
            <div>
                <TitleAndButton
                    title='Profiles'
                    buttonText='new'
                    toggleShowForm={this.toggleShowForm}                    
                />

                {
                    this.state.showForm ? (
                        <AddProfileForm
                            handleSubmit={this.handleSubmit}
                            addNewProfile={this.addNewProfile}
                            toggleShowForm={this.toggleShowForm}
                            arrayLength={this.state.profiles.length}
                        />
                    ) : (
                        <Table
                            profiles={this.state.profiles}
                            deleteProfile={this.deleteProfile}
                        />
                    )
                }
            </div>
        )
    }
}
