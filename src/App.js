import React from 'react';
import './App.css';
import Header from './components/_common/Header';
import Profiles from './components/Profiles';

const navConfig = [{
    title: 'Profili',
}, {
    title: 'Stakeholder',
}]

const App = props => {
    return (
        <div>
            <Header
                logo='/logo512.png'
                navConfig={navConfig}
            />

            <Profiles />
        </div>
    )
}

export default App;
